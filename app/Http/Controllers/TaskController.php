<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    protected $task;

    /**
     * @param $task
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    public function index(){
        $tasks = $this->task->all();
        return view('tasks.index',compact('tasks'));
    }
    public function store(CreateTaskRequest $request){
        $this->task->create($request->all());
        return redirect()->route('tasks.index');
    }
    public function create(){
        return view('tasks.create');
    }
}
