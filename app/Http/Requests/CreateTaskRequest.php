<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {

        return [
            'name' => 'required|min:6|max:255|string'
        ];
    }
    public function messages()
    {
        return [
            'name.min' => 'Phai nhap lon hon 6 ky tu',
            'name.max' => 'Phai nhap nho hon 255 ky tu',
            'name.string' => 'Nhap dang string'
        ]; // TODO: Change the autogenerated stub
    }
}
