
@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Create Task</h2>
        <div class="row justify-content-center">
            <form action="{{route('tasks.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="formGroupExampleInput">Name</label>
                    <input type="text" class="form-control" name="name" id="formGroupExampleInput" placeholder="Name">
                    @error('name')
                    <div class="alert alert-danger" role="alert">
                        {{$message}}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="formGroupExampleInput2">Content</label>
                    <input type="text" class="form-control" name="content" id="formGroupExampleInput2" placeholder="Content">
                </div>
                <button type="submit" class="btn btn-primary">Create</button>
            </form>
        </div>
    </div>
@endsection
