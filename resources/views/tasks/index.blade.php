
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
<table class="table">
    <thead class="thead-dark">
    <tr>
        <th scope="col">Id</th>
        <th scope="col">Name</th>
        <th scope="col">Content</th>
    </tr>
    </thead>
    <tbody>
    @foreach($tasks as $task)
    <tr>
        <td>{{$task->id}}</td>
        <td>{{$task->name}}</td>
        <td>{{$task->content}}</td>
    </tr>
    @endforeach
    </tbody>
</table>
    </div>
</div>
@endsection
