<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateNewTaskTest extends TestCase
{
    public function CreateTaskRoute(){
        return route('tasks.store');
    }
    /** @test  */
    public function authenticated_user_can_new_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->CreateTaskRoute(),$task);

        $response->assertStatus(302);
        $this->assertDatabaseHas('tasks',$task);
        $response->assertRedirect(route('tasks.index'));
    }

    /** @test  */
    public function unauthenticated_user_can_not_new_task()
    {
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->CreateTaskRoute(),$task);

        $response->assertRedirect('/login');
    }

    /** @test  */
    public function authenticated_user_can_not_new_task_if_name_field_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name' => null])->toArray();
        $response = $this->post($this->CreateTaskRoute(),$task);
        $response->assertSessionHasErrors('name');
    }

    /** @test  */
    public function authenticated_user_can_view_create_task_form()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->CreateTaskViewRoute());
        $response->assertViewIs('tasks.create');
    }

    /** @test  */
    public function authenticated_user_can_see_name_required_text_if_validate_error()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->CreateTaskViewRoute())->post($this->CreateTaskRoute(),$task);
        $response->assertRedirect($this->CreateTaskViewRoute());

    }

    /** @test  */
    public function unauthenticated_user_can_not_see_create_task_form()
    {
        $task = Task::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->CreateTaskViewRoute())->post($this->CreateTaskRoute(),$task);
        $response->assertRedirect('/login');

    }
    public function CreateTaskViewRoute(){
        return route('tasks.create');
    }
}
